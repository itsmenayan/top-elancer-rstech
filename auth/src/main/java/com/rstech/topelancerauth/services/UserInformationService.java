package com.rstech.topelancerauth.services;

import com.rstech.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rstech.topelancerauth.repositories.UserRepository;

import java.util.Optional;

/**
 * @author mj07yadav
 * @This  controller provide user related informations and is common for admin and core modules 
 * 
 * 
 */
@Service
public class UserInformationService {

	@Autowired
	private UserRepository userRepository;
	   
	
	/**
	 * @Desc its a service method which takes userId as parameter
	 * and returns complete User object from database
	 * 
	 * @author mj07yadav
	 * @param userId
	 * @return User object
	 *
	 */
	public User getUserPersonalDetails(Long userId) throws Exception{
		User userRequested = null;
		

			Optional<User> notSureUser	= userRepository.findById(userId);
			userRequested = new User();
			if(notSureUser.isPresent()) {
				userRequested = notSureUser.get();
			}else {
				throw new Exception("user not found");
			}
		
		return userRequested;
	}
	
}

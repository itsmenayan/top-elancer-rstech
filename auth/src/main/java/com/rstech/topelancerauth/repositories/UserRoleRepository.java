package com.rstech.topelancerauth.repositories;


import com.rstech.entities.User;
import com.rstech.entities.UserRoles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRoles, Integer>{
	
	
	public List<UserRoles> findAllByUserId(User userId);
}

package com.rstech.entities;


/**
 * @author mj07yadav
 * @Desc Enum to store RoleTypes
 */
public enum RoleTypes {
	
	ROLE_ADMIN("ROLE_ADMIN",1),
    ROLE_USER("ROLE_USER",2),
    ROLE_BUSINESS("ROLE_BUSINESS",3);

    private final String roleName;
    private final Integer roleId;

    RoleTypes(String roleName, Integer roleId){
    	this.roleId = roleId;
    	this.roleName = roleName;
    }

	public String getRoleName() {
		return roleName;
	}

	public Integer getRoleId() {
		return roleId;
	}
    
    
}

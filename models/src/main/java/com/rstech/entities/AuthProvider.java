package com.rstech.entities;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}

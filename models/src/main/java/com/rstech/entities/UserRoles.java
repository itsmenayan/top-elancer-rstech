package com.rstech.entities;

import javax.persistence.*;

/**
 * @author mj07yadav
 * @Desc Entity used to map ManyToMany mappings for User and roles tables
 * 
 */

@Entity
@Table(name="user_roles")
public class UserRoles {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer userRoleId;
	
	@OneToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "user_id")
	private User userId;
	
	@OneToOne
	@JoinColumn(name = "role_id")
	private Roles roleId;
	
	@Column(name="archive")
	private Boolean archive;
	
	public UserRoles() {
		
	}

	public UserRoles( User userId, Roles roleId, Boolean archive) {
		super();
		this.userId = userId;
		this.roleId = roleId;
		this.archive = archive;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public Roles getRoleId() {
		return roleId;
	}

	public void setRoleId(Roles roleId) {
		this.roleId = roleId;
	}

	public Integer getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}


	public Boolean getArchive() {
		return archive;
	}

	public void setArchive(Boolean archive) {
		this.archive = archive;
	}

	
	@Override
	public String toString() {
		return "UserRoles [userRoleId=" + userRoleId + ", userId=" + userId + ", roleId=" + roleId + ", archive="
				+ archive + "]";
	}
	
}
